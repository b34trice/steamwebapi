﻿using System.Runtime.Serialization;

namespace DAL.Models
{
    [DataContract]
    public class Root
    {
        [DataMember(Name = "result")]
        public Game Game;
    }
}
