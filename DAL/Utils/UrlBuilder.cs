﻿namespace DAL.Utils
{
    public class UrlBuilder
    {
        private string _baseUrl;
        private string _key;

        public UrlBuilder(string baseUrl, string key)
        {
            _baseUrl = baseUrl;
            _key = key;
        }

        public string GetUrlMatchStatistics(int matchId, string matchIdUrl, string url)
        {
            return $"{_baseUrl}{url}{matchIdUrl}{matchId}{_key}";
        }

        public string GetUrlMatchsHistory(object accountId, string accountIdUrl, string url)
        {
            return $"{_baseUrl}{url}{accountIdUrl}{accountId}{_key}";
        }
    }
}
