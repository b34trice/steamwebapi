﻿using System.Runtime.Serialization;

namespace BLL.Models
{
    [DataContract]
    public class Root
    {
        [DataMember(Name = "result")]
        public Game Game;
    }
}
