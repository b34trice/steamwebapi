﻿using DAL.Utils;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class GetHistoryApiService
    {
        private readonly HttpClient _httpClient;

        public GetHistoryApiService()
        {
            _httpClient = new HttpClient()
            {
                Timeout = new TimeSpan(0, 1, 0)
            };
        }

        public async Task<string> GetAllAsync(object accountId)
        {
            var response = await _httpClient.GetAsync(new UrlBuilder(Constants.BASE_URL, Constants.API_KEY).
                GetUrlMatchsHistory(accountId, Constants.ACCOUNT_ID, Constants.GET_MATCH_HISTORI));

            return await response.Content.ReadAsStringAsync();
        }
    }
}