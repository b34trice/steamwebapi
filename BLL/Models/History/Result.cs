﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BLL.Models.History
{
    [DataContract]
    public class Result
    {
        [DataMember(Name = "status")]
        public int Status { get; set; }

        [DataMember(Name = "num_results")]
        public int NumResults { get; set; }

        [DataMember(Name = "total_results")]
        public int TotalResults { get; set; }

        [DataMember(Name = "results_remaining")]
        public int ResultsRemaining { get; set; }

        [DataMember(Name = "matches")]
        public IEnumerable<Match> Matches { get; set; }
    }
}