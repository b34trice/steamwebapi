﻿using BLL.Models.History;
using DAL.Services;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class GetHistoryService
    {
        public async Task<Root> GetAllAsync(object accountId)
        {
            var history = await new GetHistoryApiService().GetAllAsync(accountId);

            return JsonConvert.DeserializeObject<Root>(history);
        }
    }
}
