﻿using System.Threading.Tasks;

namespace SteamWebApi
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await new GetAllData().GetData();
        }        
    }
}