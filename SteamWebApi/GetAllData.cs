﻿using BLL.Services;
using System.Threading.Tasks;

namespace SteamWebApi
{
    public class GetAllData
    {
        public async Task GetData()
        {
            var game = await new GetMatchStatisticService().GetAllAsync(27110133);
            var history = await new GetHistoryService().GetAllAsync("NeON__");
        }
    }
}
