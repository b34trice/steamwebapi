﻿namespace DAL.Utils
{
    public static class Constants
    {
        public static string BASE_URL = "https://api.steampowered.com/";
        public static string API_KEY = "&key=E8E028BAF22A12E490C4937C83E78ADE";

        public static string MATCH_ID = "match_id=";
        public static string ACCOUNT_ID = "account_id";
        public static string GET_MATCH_STATISTIC = "IDOTA2Match_570/GetMatchDetails/V1/?&";
        public static string GET_MATCH_HISTORI = "IDOTA2Match_205790/GetMatchHistory/v1/?&";
    }
}