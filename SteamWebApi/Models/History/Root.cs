﻿using System.Runtime.Serialization;

namespace SteamWebApi.Models.History
{
    [DataContract]
    public class Root
    {
        [DataMember(Name = "result")]
        public Result Result { get; set; }
    }
}