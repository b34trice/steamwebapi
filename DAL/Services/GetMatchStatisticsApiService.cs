﻿using DAL.Utils;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class GetMatchStatisticsApiService
    {
        private readonly HttpClient _httpClient;
        
        public GetMatchStatisticsApiService()
        {
            _httpClient = new HttpClient()
            {
                Timeout = new TimeSpan(0, 1, 0)
            };
        }

        public async Task<string> GetAllAsync(int matchId)
        {
            var response = await _httpClient.GetAsync(new UrlBuilder(Constants.BASE_URL, Constants.API_KEY).
                GetUrlMatchStatistics(matchId, Constants.MATCH_ID, Constants.GET_MATCH_STATISTIC));

            return await response.Content.ReadAsStringAsync();
        }
    }
}