﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SteamWebApi.Models.History
{
    [DataContract]
    public class Match
    {
        [DataMember(Name = "match_id")]
        public long MatchId { get; set; }

        [DataMember(Name = "match_seq_num")]
        public int MatchSeqNum { get; set; }

        [DataMember(Name = "start_time")]
        public int StartTime { get; set; }

        [DataMember(Name = "lobby_type")]
        public int LobbyType { get; set; }

        [DataMember(Name = "radiant_team_id")]
        public long RadiantTeamId { get; set; }

        [DataMember(Name = "dire_team_id")]
        public long DireTeamId { get; set; }

        [DataMember(Name = "players")]
        public IEnumerable<Player> Players { get; set; }
    }
}