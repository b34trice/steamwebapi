﻿using System.Runtime.Serialization;

namespace SteamWebApi.Models
{
    [DataContract]
    public class Root
    {
        [DataMember(Name = "result")]
        public Game Game;
    }
}
