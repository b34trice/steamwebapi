﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DAL.Models
{
    [DataContract]
    public class Game
    {
        [DataMember(Name = "players")]
        public IEnumerable<Player> Players { get; set; }

        [DataMember(Name = "radiant_win")]
        public bool IsRadianWin { get; set; }

        [DataMember(Name = "duration")]
        public int GameDuration { get; set; }

        [DataMember(Name = "pre_game_duration")]
        public int PreGameDuration { get; set; }

        [DataMember(Name = "start_time")]
        public int MatchStartTime { get; set; }

        [DataMember(Name = "match_id")]
        public long MatchId { get; set; }

        [DataMember(Name = "match_seq_num")]
        public int MatchSequenceNumber { get; set; }

        [DataMember(Name = "tower_status_radiant")]
        public int TowerStatusRadiant { get; set; }

        [DataMember(Name = "tower_status_dire")]
        public int TowerStatusDire { get; set; }

        [DataMember(Name = "barracks_status_radiant")]
        public int BarracksStatusRadiant { get; set; }

        [DataMember(Name = "barracks_status_dire")]
        public int BarracksStatusDire { get; set; }

        [DataMember(Name = "cluster")]
        public int Cluster { get; set; }

        [DataMember(Name = "first_blood_time")]
        public int FirstBloodTime { get; set; }

        [DataMember(Name = "lobby_type")]
        public int LobbyType { get; set; }

        [DataMember(Name = "human_players")]
        public int HumanPlayersCounter { get; set; }

        [DataMember(Name = "leagueid")]
        public int LeagueId { get; set; }

        [DataMember(Name = "positive_votes")]
        public int PositiveVotes { get; set; }

        [DataMember(Name = "negative_votes")]
        public int NegativeVotes { get; set; }

        [DataMember(Name = "game_mode")]
        public int GameMode { get; set; }

        [DataMember(Name = "flags")]
        public int Flags { get; set; }

        [DataMember(Name = "engine")]
        public int Engine { get; set; }

        [DataMember(Name = "radiant_score")]
        public int RadiantScore { get; set; }

        [DataMember(Name = "dire_score")]
        public int DireScore { get; set; }
    }
}