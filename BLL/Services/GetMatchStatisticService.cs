﻿using BLL.Models;
using DAL.Services;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class GetMatchStatisticService
    {
        public async Task<Root> GetAllAsync(int gameId)
        {
            var statistic = await new GetMatchStatisticsApiService().GetAllAsync(gameId);

            return JsonConvert.DeserializeObject<Root>(statistic);
        }
    }
}
