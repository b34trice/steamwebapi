﻿using System.Runtime.Serialization;

namespace SteamWebApi.Models
{
    [DataContract]
    public class Player
    {
        [DataMember(Name = "account_id")]
        public long AccountId { get; set; }

        [DataMember(Name = "player_slot")]
        public int PlayerSlot { get; set; }

        [DataMember(Name = "hero_id")]
        public int HeroId { get; set; }

        [DataMember(Name = "item_0")]
        public int Item0 { get; set; }

        [DataMember(Name = "item_1")]
        public int Item1 { get; set; }

        [DataMember(Name = "item_2")]
        public int Item2 { get; set; }

        [DataMember(Name = "item_3")]
        public int Item3 { get; set; }

        [DataMember(Name = "item_4")]
        public int Item4 { get; set; }

        [DataMember(Name = "item_5")]
        public int Item5 { get; set; }

        [DataMember(Name = "backpack_0")]
        public int Backpack0 { get; set; }

        [DataMember(Name = "backpack_1")]
        public int Backpack1 { get; set; }

        [DataMember(Name = "backpack_2")]
        public int Backpack2 { get; set; }

        [DataMember(Name = "kills")]
        public int Kills { get; set; }

        [DataMember(Name = "deaths")]
        public int Deaths { get; set; }

        [DataMember(Name = "assists")]
        public int Assists { get; set; }

        [DataMember(Name = "leaver_status")]
        public int LeaverStatus { get; set; }

        [DataMember(Name = "last_hits")]
        public int LastHits { get; set; }

        [DataMember(Name = "denies")]
        public int Denies { get; set; }

        [DataMember(Name = "gold_per_min")]
        public int GoldPerMin { get; set; }

        [DataMember(Name = "xp_per_min")]
        public int XpPerMin { get; set; }

        [DataMember(Name = "level")]
        public int Level { get; set; }
    }
}