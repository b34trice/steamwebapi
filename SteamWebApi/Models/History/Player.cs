﻿using System.Runtime.Serialization;

namespace SteamWebApi.Models.History
{
    [DataContract]
    public class Player
    {
        [DataMember(Name = "account_id")]
        public long AccountId { get; set; }

        [DataMember(Name = "player_slot")]
        public int PlayerSlot { get; set; }

        [DataMember(Name = "hero_id")]
        public int HeroId { get; set; }
    }
}
